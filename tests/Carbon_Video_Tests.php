<?php

use Carbon_Video\Cache\Abstract_Cache;

class Carbon_Video_Tests extends PHPUnit_Framework_TestCase {
	public $vimeo_links = array(
		'http://vimeo.com/2526536',
		'http://vimeo.com/channels/staffpicks/98861259',
		'http://vimeo.com/2526536#t=15s',
		'http://vimeo.com/2526536#t=195s',
		'https://vimeo.com/2526536/download?t=1430490026&v=8950830&s=49a858df9eb7f016593c63c60e66bd9f',
	);

	public $youtube_links = array(
		'http://youtu.be/lsSC2vx7zFQ',
		'http://youtu.be/6jCNXASjzMY?t=3m11s',
		'//www.youtube.com/embed/LlhfzIQo-L8?rel=0',
		'https://www.youtube.com/watch?v=lsSC2vx7zFQ',
		'<iframe width="560" height="315" src="//www.youtube.com/embed/LlhfzIQo-L8?rel=0" frameborder="0" allowfullscreen></iframe>',
	);

	/**
	 * YouTube Tests
	 */
	public function test_parse_youtube_links() {
		foreach ( $this->youtube_links as $youtube_link ) {
			$video = Carbon_Video\Carbon_Video::create( $youtube_link );

			$this->assertTrue( $video->parse( $youtube_link ) );
		}
	}

	public function test_get_the_type_of_youtube_links() {
		foreach ( $this->youtube_links as $youtube_link ) {
			$video = Carbon_Video\Carbon_Video::create( $youtube_link );

			$this->assertEquals( $video->get_type(), 'Youtube' );
		}
	}

	public function test_get_youtube_share_links() {
		foreach ( $this->youtube_links as $youtube_link ) {
			$video = Carbon_Video\Carbon_Video::create( $youtube_link );

			$this->hasOutput( $video->get_share_link() );
		}
	}

	public function test_get_youtube_links() {
		foreach ( $this->youtube_links as $youtube_link ) {
			$video = Carbon_Video\Carbon_Video::create( $youtube_link );

			$this->hasOutput( $video->get_link() );
		}
	}

	public function test_get_youtube_get_embed_urls() {
		foreach ( $this->youtube_links as $youtube_link ) {
			$video = Carbon_Video\Carbon_Video::create( $youtube_link );

			$this->hasOutput( $video->get_embed_url() );
		}
	}

	public function test_get_youtube_get_embed_codes() {
		foreach ( $this->youtube_links as $youtube_link ) {
			$video = Carbon_Video\Carbon_Video::create( $youtube_link );

			$this->hasOutput( $video->get_embed_code() );
		}
	}

	public function test_get_youtube_titles() {
		foreach ( $this->youtube_links as $youtube_link ) {
			$video = Carbon_Video\Carbon_Video::create( $youtube_link );

			$video->set_api_key( 'AIzaSyDdQl6gnYd50hGxfwEaCB2wixLvBuCsaAw' );

			$this->hasOutput( $video->get_title() );
		}
	}

	public function test_get_youtube_images() {
		foreach ( $this->youtube_links as $youtube_link ) {
			$video = Carbon_Video\Carbon_Video::create( $youtube_link );

			$video->set_api_key( 'AIzaSyDdQl6gnYd50hGxfwEaCB2wixLvBuCsaAw' );

			$this->hasOutput( $video->get_image() );
		}
	}

	public function test_get_youtube_thumbnails() {
		foreach ( $this->youtube_links as $youtube_link ) {
			$video = Carbon_Video\Carbon_Video::create( $youtube_link );

			$video->set_api_key( 'AIzaSyDdQl6gnYd50hGxfwEaCB2wixLvBuCsaAw' );

			$this->hasOutput( $video->get_thumbnail() );
		}
	}

	/**
	 * Vimeo Tests
	 */
	public function test_parse_vimeo_links() {
		foreach ( $this->vimeo_links as $vimeo_link ) {
			$video = Carbon_Video\Carbon_Video::create( $vimeo_link );

			$this->assertTrue( $video->parse( $vimeo_link ) );
		}
	}

	public function test_flush_vimeo_links_data() {
		foreach ( $this->vimeo_links as $vimeo_link ) {
			$video = Carbon_Video\Carbon_Video::create( $vimeo_link );

			$this->assertTrue( $video->flush_video_cache() );
		}
	}

	public function test_get_the_type_of_vimeo_links() {
		foreach ( $this->vimeo_links as $vimeo_link ) {
			$video = Carbon_Video\Carbon_Video::create( $vimeo_link );

			$this->assertEquals( $video->get_type(), 'Vimeo' );
		}
	}

	public function test_get_vimeo_links_thumbnail() {
		foreach ( $this->vimeo_links as $vimeo_link ) {
			$video = Carbon_Video\Carbon_Video::create( $vimeo_link );

			$this->hasOutput( $video->get_thumbnail() );
		}
	}

	public function test_get_vimeo_links_image() {
		foreach ( $this->vimeo_links as $vimeo_link ) {
			$video = Carbon_Video\Carbon_Video::create( $vimeo_link );

			$this->hasOutput( $video->get_image() );
		}
	}

	public function test_get_vimeo_share_links() {
		foreach ( $this->vimeo_links as $vimeo_link ) {
			$video = Carbon_Video\Carbon_Video::create( $vimeo_link );

			$this->hasOutput( $video->get_share_link() );
		}
	}

	public function test_get_vimeo_links() {
		foreach ( $this->vimeo_links as $vimeo_link ) {
			$video = Carbon_Video\Carbon_Video::create( $vimeo_link );

			$this->hasOutput( $video->get_link() );
		}
	}

	public function test_get_vimeo_embed_url() {
		foreach ( $this->vimeo_links as $vimeo_link ) {
			$video = Carbon_Video\Carbon_Video::create( $vimeo_link );

			$this->hasOutput( $video->get_embed_url() );
		}
	}

	public function test_get_vimeo_embed_code() {
		foreach ( $this->vimeo_links as $vimeo_link ) {
			$video = Carbon_Video\Carbon_Video::create( $vimeo_link );

			$this->hasOutput( $video->get_embed_code() );
		}
	}

	public function test_get_vimeo_titles() {
		foreach ( $this->vimeo_links as $vimeo_link ) {
			$video = Carbon_Video\Carbon_Video::create( $vimeo_link );

			$this->hasOutput( $video->get_title() );
		}
	}
}
