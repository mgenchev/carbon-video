# Carbon Video

Video utility library. The aim of the library is to ease parsing of various
video inputs and to provide consistent interface to work with YouTube and Vimeo
videos that doesn’ t require understanding of all types of URL and embed formats
provided by video providers.

Documentation is available at wp-wiki.

## Installation

The library is available as Composer package. You can include it in your Project with:

`composer require htmlburger/carbon-video`

## Usage:

`
$youtube_link = "https://www.youtube.com/watch?v=n4RjJKxsamQ";

$video = Carbon_Video\Carbon_Video::create( $youtube_link );`

Print HTML video embed code with specified dimensions
`php echo $video->get_embed_code( 640, 480 );`

Remove related videos and print embed code
`php echo $video->set_param( 'rel', '0' )->get_embed_code();`

Returns share link for the video
`$video->get_share_link();`

Returns the video link
`$video->get_link();`

Print video thumbnail
`echo $video->get_thumbnail();`

Print large video thumbnail
`echo $video->get_image();`

Print video type
`echo $video->get_type();`

Set video width
`echo $video->set_width( 640 );`

Get video width
`echo $video->get_width();`

Set video height
`echo $video->set_height( 480 );`

Get video height
`echo $video->get_height();`



## Vimeo special methods

Clears the cached video data - like thumbnail and image
`$video->flush_video_cache();`

Get video title
`$video->get_title();`

## YouTube special methods

Set YouTube API Key - The API Key is required if you need to get the video title.
`$video->set_api_key();`

Get video title - First you need to set the API Key.
`$video->get_title();`

### How to get the YouTube API Key

1. Go to https://developers.google.com/ and log in or create an account.
2. Go to https://console.developers.google.com/project and CREATE PROJECT.
3. Then click on the link option called “YouTube Data API.”
4. Click on the “ENABLE” button.
5. Click on the blue ‘Go to Credentials’ button.
6. Choose the select options YouTube Data API v3 and Web server. Then choose Public data and press “What credentials do I need?.”
7. Google will create your new project and you can copy your API Key.


# Supported video links:

## Vimeo
http://vimeo.com/2526536
http://vimeo.com/channels/staffpicks/98861259
http://vimeo.com/2526536#t=15s
http://vimeo.com/2526536#t=195s
https://vimeo.com/2526536/download?t=1430490026&v=8950830&s=49a858df9eb7f016593c63c60e66bd9f

## YouTube
http://youtu.be/lsSC2vx7zFQ
http://youtu.be/6jCNXASjzMY?t=3m11s
//www.youtube.com/embed/LlhfzIQo-L8?rel=0
https://www.youtube.com/watch?v=lsSC2vx7zFQ
<iframe width="560" height="315" src="//www.youtube.com/embed/LlhfzIQo-L8?rel=0" frameborder="0" allowfullscreen></iframe>

Youtube old embed flash code:
<object width="234" height="132"><param name="movie" ...... type="application/x-shockwave-flash" width="234" heighGt="132" allowscriptaccess="always" allowfullscreen="true"></embed></object>
