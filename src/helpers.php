<?php

if ( ! function_exists( 'crb_filter_video' ) ) {
	/**
	 * Filters/resizes video embed codes.
	 * @param  string  embed code provided by video embedding service
	 * @param  boolean $wmode
	 * @param  mixed $width
	 * @param  mixed $height
	 * @return string
	 */
	function crb_filter_video( $html, $wmode = false, $width = false, $height = false ) {
		return Carbon_Video\Carbon_Video::create( $html )
		    ->get_embed_code( $width, $height );
	}
}

if ( ! function_exists( 'crb_get_video_thumb' ) ) {
	/**
	 * Return the thumbnail src for Youtube and Vimeo videos
	 * @param  string $embed_code  the full video embed code ( or YouTube video url )
	 * @return string URL to the thumbnail
	 */
	function crb_get_video_thumb( $embed_code ) {
		return Carbon_Video\Carbon_Video::create( $embed_code )->get_thumbnail();
	}
}

if ( ! function_exists( 'crb_get_vimeo_thumb' ) ) {
	/**
	 * Return the thumbnail src for Vimeo videos
	 * @param  mixed Vimeo video id
	 * @return string URL to the thumbnail
	 */
	function crb_get_vimeo_thumb( $videoid ) {
		return Carbon_Video\Carbon_Video::create( $videoid )->get_thumbnail();
	}
}

if ( ! function_exists( 'crb_get_youtube_video' ) ) {
	/**
	 * Return a URL to an embedabble YouTube Video (the actual video file URL)
	 * @param  string $video_url
	 * @return string
	 */
	function crb_get_youtube_video( $video_url ) {
		$video = Carbon_Video\Carbon_Video::create( $video_url );

		return $video->get_embed_url();
	}
}

if ( ! function_exists( 'crb_create_embedcode' ) ) {
	/**
	 * Builds embed code from a video URL
	 * @param  string  $video_url
	 * @param  integer $width
	 * @param  integer $height
	 * @param  boolean $old_embed_code Whether to produce a mobile devices compitable, iframe-based, code(new code) or flash based embed code(old code)
	 * @param  boolean $autoplay
	 * @return [type]
	 */
	function crb_create_embedcode( $video_url, $width = 440, $height = 350, $old_embed_code = false, $autoplay = false ) {
		$video = Carbon_Video\Carbon_Video::create( $video_url );

		if ( $autoplay ) {
			$video->set_param( 'autoplay', 1 );
		}

		return $video->get_embed_code( $width, $height );
	}
}

if ( ! function_exists( 'crb_create_youtube_embedcode' ) ) {
	/**
	 * Generates an embedcode of a YouTube Video.
	 * @param  string  $video_url URL of the playable YouTube Video (for example: http://www.youtube.com/watch?v=emMDmRtdP7w0)
	 * @param  integer $width width of embedded video (optional)
	 * @param  integer $height height of embedded video (optional)
	 * @param  boolean $old_embed_code whether to use the old embedcode (optional). Uses @get_youtube_video to grab embeddable video URL
	 * @param  boolean $autoplay
	 * @return [type]
	 */
	function crb_create_youtube_embedcode( $video_url, $width = 440, $height = 350, $old_embed_code = false, $autoplay = false ) {
		$video = Carbon_Video\Carbon_Video::create( $video_url );

		if ( $autoplay ) {
			$video->set_param( 'autoplay', 1 );
		}

		return $video->get_embed_code( $width, $height );
	}
}

if ( ! function_exists( 'crb_create_vimeo_embedcode' ) ) {
	/**
	 * Return an embedcode of a Vimeo Video.
	 * @param  string  $video_url URL of the playable Vimeo Video (for example: http://vimeo.com/29081264)
	 * @param  integer $width width of embedded video (optional)
	 * @param  integer $height height of embedded video (optional)
	 * @param  boolean $autoplay
	 * @return [type]
	 */
	function crb_create_vimeo_embedcode( $video_url, $width=440, $height=350, $autoplay = false ) {
		$video = Carbon_Video\Carbon_Video::create( $video_url );

		if ( $autoplay ) {
			$video->set_param( 'autoplay', 1 );
		}

		return $video->get_embed_code( $width, $height );
	}
}
