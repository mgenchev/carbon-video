<?php

namespace Carbon_Video\Video;

/**
 * Class for broken videos.
 */
class Broken extends Video {
	public function parse( $video_code ) {
		return true;
	}
	public function get_link() {
		return false;
	}
	public function get_share_link() {
		return false;
	}
	public function get_embed_url() {
		return false;
	}
	public function get_embed_code( $width = null, $height = null ) {
		return false;
	}
	public function get_thumbnail() {
		return false;
	}
	public function get_image() {
		return false;
	}
	public function get_title() {
		return false;
	}
	public function set_api_key() {
		return false;
	}
}
