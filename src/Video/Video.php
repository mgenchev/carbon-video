<?php

namespace Carbon_Video\Video;

use Carbon_Video\Cache\Abstract_Cache;

/*
 * Defines the interface for working with video embeds and provides factory method for
 * creating new objects(`create`).
 */
abstract class Video {

	/**
	 * Width and height container
	 * @var array
	 */
	protected $dimensions = array(
		'width'  => null,
		'height' => null,
	);

	/**
	 * Confing.
	 *
	 * @static
	 *
	 * @var array
	 */
	public static $config = [
		'vimeo_lifetime'  => 300,
		'cache_driver'    => [ 'WordPress', 'File' ],
		'youtube_api_key' => NULL,
	];

	/**
	 * Cache Driver.
	 *
	 * @var Abstract_Cache
	 */
	protected $cache_driver = null;

	/**
	 * Cache Drivers.
	 *
	 * @static
	 *
	 * @var array
	 */
	protected static $cache_drivers = [ 'WordPress', 'File' ];

	/**
	 * Cache lifetime in seconds. Defaults to 300 seconds (5 minutes).
	 *
	 * @static
	 *
	 * @var integer
	 */
	public static $cache_lifetime = 300;

	/**
	 * Http-related object
	 * @var Carbon_Video_Http
	 */
	public $http;

	/**
	 * The ID of the video in the site that hosts it
	 * @var string
	 */
	protected $video_id;

	/**
	 * URL GET params.
	 * @var array
	 */
	protected $params = array();

	/**
	 * The time that video should start playback at
	 * @var boolean|integer
	 */
	protected $start_time = false;

	/**
	 * The video type - YouTube or Vimeo
	 * @var string
	 */
	protected $video_type;

	/**
	 * Commonly used fragments in the parsing regular expressions
	 * @var array
	 */
	protected $regex_fragments = array(
		// Describe "http://" or "https://" or "//"
		'protocol' => '(?:https?:)?//',

		// Describe GET args list
		'args' => '(?:\?(?P<params>.+?))?',
	);

	/**
	 * Handles the paring of all formats associated with the partuclar video provider
	 *
	 * @param  string $video_code URL, embed code, etc.
	 * @return bool             whether the operations has succeeded
	 */
	abstract public function parse( $video_code );

	/**
	 * Return link to the video page's at the provider site
	 * @return string
	 */
	abstract public function get_link();

	/**
	 * Returns short link for the video at the provider site
	 * @return string
	 */
	abstract public function get_share_link();

	/**
	 * Returns iframe-based embed code with the specified dimensions
	 * @param  int $width
	 * @param  int $height
	 * @return string
	 */
	abstract public function get_embed_code( $width = null, $height = null );

	/**
	 * Return direct URL to the iframe embed(without the iframe tag HTML)
	 * @return string URL to youtube embed
	 */
	abstract public function get_embed_url();

	/**
	 * Return URL of image thumbnail for the current video
	 * @return string
	 */
	abstract public function get_thumbnail();

	/**
	 * Return URL of big image for the current video
	 * @return string
	 */
	abstract public function get_image();

	/**
	 * Return the title of current video
	 * @return string
	 */
	abstract public function get_title();

	public function __construct() {
		$this->init_cache();
		$this->http = new HTTP();
	}

	/**
	 * Sets up the Cache Driver.
	 *
	 * @access protected
	 *
	 * @return void
	 */
	protected function init_cache() {
		foreach ( static::$cache_drivers as $candidate ) {
			$cache_class_name = 'Carbon_Video\\Cache\\' . $candidate . '_Cache';

			if ( $cache_class_name::test() ) {
				$this->set_cache_driver( new $cache_class_name( static::$cache_lifetime ) );
				break;
			}
		}
	}

	/**
	 * Sets the Cache Driver.
	 *
	 * @access public
	 *
	 * @param Abstract_Cache $cache
	 */
	protected function set_cache_driver( Abstract_Cache $cache ) {
		$this->cache_driver = $cache;
		return $this;
	}
	/**
	 * Returns the Cache Driver.
	 *
	 * @access public
	 *
	 * @return Abstract_Cache|null
	 */
	protected function get_cache_driver() {
		return $this->cache_driver;
	}

	public function is_broken() {
		return empty( $this->video_id );
	}

	public function get_width() {
		return $this->dimensions['width'];
	}

	public function set_width( $new_width ) {
		$this->dimensions['width'] = $new_width;

		return $this;
	}

	public function get_height() {
		return $this->dimensions['height'];
	}

	public function set_height( $new_height ) {
		$this->dimensions['height'] = $new_height;

		return $this;
	}

	public function get_type() {
		return $this->video_type;
	}

	public function get_param( $arg ) {
		if ( isset( $this->params[ $arg ] ) ) {
			return $this->params[ $arg ];
		}
		return null;
	}

	public function set_param( $arg, $val ) {
		$this->params[ $arg ] = $val;
		return $this;
	}

	public function get_id() {
		return $this->video_id;
	}

	public function get_start_time() {
		return $this->start_time;
	}

	/**
	 * Set multiple parameters in one call
	 * @param array $params associative array where keys are param
	 *                      names and values are param values
	 */
	public function set_params( $params ) {
		foreach ( $params as $param_name => $param_val ) {
			$this->set_param( $param_name, $param_val );
		}
		return $this;
	}

	// If width and height are not provided in the function parameters,
	// get them from the initial video code; if the object wasn't constructed
	// from an embed code(and doesn't have initial width and height), use
	// the default, hard-coded dimensions.

	protected function get_embed_width( $user_supplied_width ) {
		if ( ! is_null( $user_supplied_width ) ) {
			return $user_supplied_width;
		}
		if ( ! empty( $this->dimensions['width'] ) ) {
			return $this->dimensions['width'];
		}
		return $this->default_width;
	}

	public function get_embed_height( $user_supplied_height ) {
		if ( ! is_null( $user_supplied_height ) ) {
			return $user_supplied_height;
		}
		if ( ! empty( $this->dimensions['height'] ) ) {
			return $this->dimensions['height'];
		}
		return $this->default_height;
	}
}
