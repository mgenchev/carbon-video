<?php

namespace Carbon_Video\Video;

/**
 * Simple class that forwards requests.
 * Allows testing.
 */
class HTTP {
	public function get( $url ) {
		$client = new \GuzzleHttp\Client();
		$response = $client->request( 'GET', $url );

		return $response->getBody();
	}
}
