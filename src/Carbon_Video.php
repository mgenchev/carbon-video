<?php

namespace Carbon_Video;

class Carbon_Video {
	/**
	 * Cache Drivers.
	 *
	 * @static
	 *
	 * @var array
	 */
	protected static $video_providers = [ 'YouTube', 'Vimeo' ];

	/**
	 * Parses embed code, url, or video ID and creates new object based on it.
	 *
	 * @param string $video embed code, url, or video ID
	 * @return object Carbon_Video
	 **/
	static function create( $video_code ) {
		$video_code = trim( $video_code );

		$video = null;

		foreach ( self::$video_providers as $video_provider ) {
			$class_name = 'Carbon_Video\\Video\\' . $video_provider;

			if ( call_user_func( array( $class_name, 'test' ), $video_code ) ) {
				$video = new $class_name;
 				break;
			}
		}

		if ( is_null( $video ) ) {
			// No video provider recognized the video
			$video = new \Carbon_Video\Video\Broken();
		}

		$result = $video->parse( $video_code );

		if ( ! $result ) {
			// Couldn't parse the video code.
			$video = new \Carbon_Video\Video\Broken();
		}

		return $video;
	}
}
